import React from 'react';
import {NavLink} from "react-router-dom";
import '../../App.css';

const Header = () => {
    return (
        <div className="nav">
            <span>My Blog</span>
            <ul>
                <li><NavLink to={'/'}>Home</NavLink></li>
                <li><NavLink to={'/form'}>Add</NavLink></li>
                <li><NavLink to={'/about-us'}>About</NavLink></li>
                <li><NavLink to={'/contacts'}>Contacts</NavLink></li>
            </ul>
        </div>
    );
};

export default Header;