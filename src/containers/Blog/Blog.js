import React, {useState, useEffect} from 'react';
import './Blog.css';
import Post from "../Post/Post";
import axiosPosts from "../../axios-posts";

const Blog = props => {

    const [posts, setPosts] = useState({});

    useEffect(() => {
        const fetchData = async () => {
            const postsResponse = (await axiosPosts.get('/posts.json')).data;
            setPosts(postsResponse);
        }

        fetchData();
    }, [])

    return (
        <div className="blog">
            <>
                {Object.keys(posts).map((key, index) => {
                    return <Post id={key}
                                 // changePage={props.history}
                                 key={key}
                                 id={key}
                                 time={posts[key].dateTime}
                                 title={posts[key].title}
                                 description={posts[key].description}
                    />
                })}
            </>
        </div>
    );
};

export default Blog;