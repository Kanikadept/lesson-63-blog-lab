import React, {useEffect, useState} from 'react';
import './SinglePost.css';
import axiosPosts from "../../../axios-posts";


const SinglePost = props => {

    const [singlePost, setSinglePost] = useState(null);

    useEffect(() => {
        const fetchData = async () => {
            const postResponse = await axiosPosts.get('/posts/' + props.match.params.id + '.json');
            setSinglePost(postResponse.data);
        }

        fetchData().catch(console.error);
    }, [props.match.params.id])


    const handleDelete = () => {
        const fetchData = async () => {
            await axiosPosts.delete('/posts/' + props.match.params.id + '.json');
            props.history.push('/');
        }

       fetchData().catch(console.error);
    }

    const handleEdit = () => {
        props.history.push('/posts/'+ props.match.params.id +'/edit');
    }

    return (
        singlePost && <div className="single-post">
            <div className="content">
                <p>{singlePost.dateTime}</p>
                <p>{singlePost.title}</p>
                <p>{singlePost.description}</p>
            </div>
            <div className="btns">
                <button onClick={handleEdit}>Edit</button>
                <button onClick={handleDelete}>Delete</button>
            </div>
        </div>
    );
};

export default SinglePost;