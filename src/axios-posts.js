import axios from "axios";

const axiosPosts = axios.create({
    baseURL: 'https://lesson-63-blog-kani-default-rtdb.firebaseio.com/'
});

export default axiosPosts;