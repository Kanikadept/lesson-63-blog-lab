import React, {useState} from 'react';
import './PostForm.css';
import axiosPosts from "../../../axios-posts";


const PostForm = props => {

    const [formData, setFormData] = useState({
        title: '',
        description: '',
        dateTime: ''
    })


    const handleFormDataChange = (event) => {
        const {name, value} = event.target;

        setFormData(formData => ({
            ...formData,
            [name]: value, dateTime: new Date().toLocaleString()
        }));
    };

    const handleSubmit = async (event) => {
        event.preventDefault();

        try {
            await axiosPosts.post('/posts.json', formData);
            props.history.push('/')
        } finally {

        }

        setFormData({
            title: '',
            description: ''
        });
    }

    return (
        <div className="post-form">
            <p>Add new post</p>

            <form onSubmit={handleSubmit}>
                <label>Title</label>
                <input value={formData.title} onChange={handleFormDataChange} name="title" type="text" required/>

                <label className="description-lab">Description</label>
                <textarea value={formData.description} onChange={handleFormDataChange} name="description" required/>
                <button>Save</button>
            </form>
        </div>
    );
};

export default PostForm;