import React, {useState, useEffect} from 'react';
import './PostEditForm.css';
import axiosPosts from "../../../axios-posts";

const PostEditForm = props => {
    const [formData, setFormData] = useState({
        title: '',
        description: '',
        dateTime: ''
    })

    useEffect(() => {

        const fetchData = async () => {
            const postResponse = await axiosPosts.get('/posts/' + props.match.params.id + '.json');
            setFormData(postResponse.data);
        }
        fetchData().catch(console.error);

    }, [props.match.params.id])

    const handleFormDataChange = (event) => {
        const {name, value} = event.target;

        setFormData(formData => ({
            ...formData,
            [name]: value, dateTime: new Date().toLocaleString()
        }));
    };

    const handleSubmit = async (event) => {
        event.preventDefault();

        try {
            await axiosPosts.put('/posts/' + props.match.params.id + '.json', formData);
        } finally {

        }

        setFormData({
            title: '',
            description: ''
        });

        props.history.push('/');
    }

    return (
        <div className="post-edit-form">
            <p>Edit post</p>
            <form onSubmit={handleSubmit}>
                <label>Title</label>
                <input value={formData.title} onChange={handleFormDataChange} name="title" type="text" required/>

                <label className="description-lab">Description</label>
                <textarea value={formData.description} onChange={handleFormDataChange} name="description" required/>
                <button>Edit</button>
            </form>
        </div>
    );
};

export default PostEditForm;