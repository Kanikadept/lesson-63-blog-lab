import './App.css';
import {Switch, Route, NavLink} from "react-router-dom";
import Blog from "./containers/Blog/Blog";
import PostForm from "./containers/Post/PostForm/PostForm";
import SinglePost from "./containers/Post/SinglePost/SinglePost";
import PostEditForm from "./containers/Post/PostEditForm/PostEditForm";
import Header from "./components/UI/Header";
import AboutUs from "./components/UI/AboutUs/AboutUs";
import Contacts from "./components/UI/Contacts/Contacts";

const App = () => {

    return (
        <div className="App">
            <div className="container">
                <Header/>
                <Switch>
                    <Route path="/" exact component={Blog}/>
                    <Route path="/form" component={PostForm}/>
                    <Route path="/about-us" component={AboutUs}/>
                    <Route path="/contacts" component={Contacts}/>
                    <Route path="/posts/:id/edit" component={PostEditForm}/>
                    <Route path="/posts/:id" render={props => (
                        <SinglePost {...props}/>
                    )}/>
                </Switch>
            </div>
        </div>
    );
}

export default App;
