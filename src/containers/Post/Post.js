import React from 'react';
import {NavLink} from 'react-router-dom';
import './Post.css';

const Post = props => {

    // const handleRoute = () => {
    //     props.changePage.push({
    //         pathname: '/posts/' + props.id,
    //     });
    // };

    return (
        <div className="post">
            <p>{props.time}</p>
            <p>{props.title}</p>
            <button><NavLink to={`/posts/${props.id}`}>Read More >></NavLink></button>
        </div>
    );
};

export default Post;